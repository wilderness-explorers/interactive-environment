﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputHandler : MonoBehaviour
{
    public InteractionInput interactionInput;
    public InspectionController inspectionController;
    public GameObject inspectionCanvas;

    void OnEnable()
    {
        interactionInput.ResetState();
    }

    void Update()
    {
        GetInteractionInput();
    }

    private void GetInteractionInput()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            inspectionController.TurnOffInspection();
            inspectionCanvas.SetActive(false);
        }
    }

    public void QuitApplication()
    {
        Application.Quit();
    }
}