﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ObjectSelectionUI : MonoBehaviour
{
    [SerializeField]
    private Image selectionIndicator;
    
    public void SelectionProgress(float fillValue)
    {
        selectionIndicator.fillAmount = fillValue;
    }

    public void Reset()
    {
        selectionIndicator.fillAmount = 0.0f;
    }
}