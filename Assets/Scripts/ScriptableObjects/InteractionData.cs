﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Interaction Data", menuName = "InteractionSystem/InteractionData")]
public class InteractionData : ScriptableObject
{
    //Reference to current interactables.
    private Interactables interactable;
    public Interactables Interactables
    {
        get
        {
            return interactable;
        }
        set
        {
            interactable = value;
        }
    }

    public void Interact()
    {
        interactable.OnInteract();
        ResetData();
    }

    //To check if the current interactable is the same as the previous one.
    //To avoid multiple detection of the same object, by the raycast.
    public bool IsSameObject(Interactables newInteractable)
    {
        return (interactable == newInteractable);
    }

    //To check if the current interactable is empty.
    public bool IsEmpty()
    {
        return (interactable == null);
    }

    //To be able to interact with new object.
    public void ResetData()
    {
        interactable = null;
    }
}