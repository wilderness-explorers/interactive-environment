﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "InteractionInput", menuName = "InteractionSystem/InputData")]
public class InteractionInput : ScriptableObject
{
    private bool isSelected;
    public bool IsSelected
    {
        get
        {
            return isSelected;
        }
        set
        {
            isSelected = value;
        }
    }

    private bool isReleased;
    public bool IsReleased
    {
        get
        {
            return isReleased;
        }
        set
        {
            isReleased = value;
        }
    }

    public void ResetState()
    {
        isSelected = false;
        isReleased = false;
    }
}