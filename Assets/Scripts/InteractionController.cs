﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractionController : MonoBehaviour
{
    [Header("Data")]
    public InteractionInput interactionInput;
    public InteractionData interactionData;

    [Space]
    [Header("Ray settings")]
    public float rayDistance;
    //Spherecast for more accuracy.
    public float raySphereRadius;
    public LayerMask interactableLayer;

    [Space]
    [Header("UI")]
    [SerializeField]
    private ObjectSelectionUI objectSelectionUI;

    private Camera cam;

    public bool debugRay = false;

    private bool isInteracting;

    private float holdTime = 0.0f;

    private void Awake()
    {
        cam = FindObjectOfType<Camera>();
    }

    private void Update()
    {
        CheckForInteractableObject();
        CheckForInteractableInput();
    }

    private void CheckForInteractableObject()
    {
        Ray ray = new Ray(cam.transform.position, cam.transform.forward);

        RaycastHit hitInfo;

        bool isHit = Physics.SphereCast(ray, raySphereRadius, out hitInfo, rayDistance, interactableLayer);

        if(isHit)
        {
            //To check if the object hit, is interactable or not.
            Interactables interactable = hitInfo.transform.GetComponent<Interactables>();

            if(interactable != null)
            {
                if(interactionData.IsEmpty())
                {
                    //Data slot empty.
                    interactionData.Interactables = interactable;
                    
                    //To start hold timer.
                    interactionInput.IsSelected = true;
                    interactionInput.IsReleased = false;
                }
                else
                {
                    //To check if a new object selected.
                    if (!interactionData.IsSameObject(interactable))
                    {
                        interactionData.Interactables = interactable;

                        //To start hold timer.
                        interactionInput.IsSelected = true;
                        interactionInput.IsReleased = false;
                    }
                }
            }
        }
        else
        {
            //Reset data if no interactable is hit.
            interactionData.ResetData();
            objectSelectionUI.Reset();

            //To stop hold timer.
            interactionInput.IsSelected = false;
            interactionInput.IsReleased = true;
        }

        //Visible rayline for debug.
        if(debugRay)
        {
            Debug.DrawRay(ray.origin, ray.direction * rayDistance, isHit ? Color.green : Color.red);
        }
    }

    private void CheckForInteractableInput()
    {
        if(interactionData.IsEmpty())
        {
            Debug.Log("Not an interactable item.");
            holdTime = 0.0f;
            objectSelectionUI.Reset();
            return;
        }

        //Check for user input.
        if(interactionInput.IsSelected)
        {
            Debug.Log("Interactable item.");
            isInteracting = true;
        }

        if(interactionInput.IsReleased)
        {
            Debug.Log("Not an interactable item.");
            isInteracting = false;
            holdTime = 0.0f;
            objectSelectionUI.Reset();
        }

        if(isInteracting)
        {
            if(interactionData.Interactables.isInteractable)
            {
                //If user gaze is held on object for sometime.
                if(interactionData.Interactables.holdInteract)
                {
                    holdTime += Time.deltaTime;

                    //To set selection UI fill amount: Hold time that is elapsed. value 0 to 1.
                    float normalizedHoldTime = holdTime / interactionData.Interactables.holdDuration;
                    objectSelectionUI.SelectionProgress(normalizedHoldTime);

                    if (holdTime >= interactionData.Interactables.holdDuration)
                    {
                        Debug.Log("Interactable item selected!");
                        interactionData.Interact();
                        isInteracting = false;
                        holdTime = 0.0f;
                    }
                }
            }
            else
            {
                return;
            }
        }
    }
}