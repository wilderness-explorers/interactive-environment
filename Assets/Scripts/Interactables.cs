﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactables : MonoBehaviour, IInteractable
{
    [Header("Interaction Settings:")]
    public float holdDuration;
    public bool holdInteract;
    public bool isInteractable;

    public GameObject inspectionObject;
    public InspectionController inspectionController;
    public int index;

    public float HoldDuration
    {
        get
        {
            return holdDuration;
        }
    }

    public bool HoldInteract
    {
        get
        {
            return holdInteract;
        }
    }

    public bool IsInteractable
    {
        get
        {
            return isInteractable;
        }
    }

    public virtual void OnInteract()
    {
        Debug.Log("Object interacted: " + gameObject.name);

        inspectionObject.SetActive(true);
        inspectionController.TurnOnInspection(index);
    }
}