﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class InspectionController : MonoBehaviour
{
    public GameObject[] inspectionObjects;
    private RigidbodyFirstPersonController firstPersonController;
    private InteractionController interactionController;
    private int currentIndex;

    private Vector3 previousFramePosition;
    public Camera inspectionCamera;
    [Tooltip("To reset camera zoom.")]
    [SerializeField]
    private float defaultFieldOfView = 60.0f;

    private bool isInspecting = false;

    [Tooltip("Speed of rotation of the model.")]
    [SerializeField]
    private float rotateSpeed = 0.1f;

    [Tooltip("Move speed of the model.")]
    [SerializeField]
    private float panSpeed = 0.005f;
    private Vector2 screenBoundary;

    private void Awake()
    {
        firstPersonController = FindObjectOfType<RigidbodyFirstPersonController>();
        interactionController = FindObjectOfType<InteractionController>();
    }

    private void OnEnable()
    {
        isInspecting = false;
    }

    private void Update()
    {
        Rotate();
        Zoom();
        Pan();
    }

    private void LateUpdate()
    {
        ClampPosition();
    }

    private void Rotate()
    {
        if(isInspecting)
        {
            //Frame when left mouse button is pressed.
            if (Input.GetMouseButtonDown(0))
            {
                previousFramePosition = Input.mousePosition;
            }

            //If user holds left mouse button.
            if (Input.GetMouseButton(0))
            {
                //Current position - Previous position.
                var positionChange = Input.mousePosition - previousFramePosition;

                //Updates previous position to current position.
                previousFramePosition = Input.mousePosition;

                //Axis of rotation for the model = position change(vector) * -90 degrees.
                var axis = Quaternion.AngleAxis(-90f, Vector3.forward) * positionChange;

                inspectionObjects[currentIndex].transform.rotation = Quaternion.AngleAxis(positionChange.magnitude * rotateSpeed, axis) * inspectionObjects[currentIndex].transform.rotation;
            }
        }
    }

    private void Zoom()
    {
        if(isInspecting)
        {
            //Zoom in.
            if (Input.GetAxis("Mouse ScrollWheel") > 0)
            {
                inspectionCamera.fieldOfView--;
            }

            //Zoom out.
            if (Input.GetAxis("Mouse ScrollWheel") < 0)
            {
                inspectionCamera.fieldOfView++;
            }
        }        
    }

    private void Pan()
    {
        if (isInspecting)
        {
            //Frame when right mouse button is pressed.
            if (Input.GetMouseButtonDown(1))
            {
                previousFramePosition = Input.mousePosition;
            }

            //If user holds right mouse button.
            if (Input.GetMouseButton(1))
            {
                //Current position - Previous position.
                var positionChange = Input.mousePosition - previousFramePosition;

                //Updates previous position to current position.
                previousFramePosition = Input.mousePosition;

                inspectionObjects[currentIndex].transform.position += (positionChange * panSpeed);
            }
        }
    }

    private void ClampPosition()
    {
        if (isInspecting)
        {
            if (Input.GetMouseButtonUp(1))
            {
                //To find screen boundary in world space. x and y values will be half of screen width and height in world units respectively.
                screenBoundary = inspectionCamera.GetComponent<Camera>().ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, inspectionObjects[currentIndex].transform.position.z));
                
                //To alter object's x and y coordinates.
                Vector3 currentPosition = inspectionObjects[currentIndex].transform.position;

                Debug.Log("screenBoundary.x " + screenBoundary.x.ToString());
                Debug.Log("screenBoundary.y " + screenBoundary.y.ToString());

                if(currentPosition.x > (Mathf.Abs(screenBoundary.x) / 60) || currentPosition.x < -(Mathf.Abs(screenBoundary.x) / 50) || currentPosition.y > (Mathf.Abs(screenBoundary.y) / 80) || currentPosition.y < -(Mathf.Abs(screenBoundary.y) / 80))
                {
                    //Clamp x position
                    //currentPosition.x = Mathf.Clamp(currentPosition.x, screenBoundary.x / 5, (screenBoundary.x / 5) * -1);
                    currentPosition.x = 0;

                    //Clamp y position
                    //currentPosition.y = Mathf.Clamp(currentPosition.y, screenBoundary.y / 5, (screenBoundary.y / 5) * -1);
                    currentPosition.y = 0;

                    inspectionObjects[currentIndex].transform.position = currentPosition;
                }
            }
        }
    }

    //Turn off user movement and  object selection in Inspection mode and vice versa.
    public void TurnOnInspection(int index)
    {
        currentIndex = index;
        inspectionObjects[currentIndex].SetActive(true);

        if(firstPersonController != null && firstPersonController.enabled == true)
        {
            firstPersonController.enabled = false;
        }

        if (interactionController != null && interactionController.enabled == true)
        {
            interactionController.enabled = false;
        }

        isInspecting = true;
        //Enable cursor to inspect.
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
    }

    public void TurnOffInspection()
    {
        inspectionObjects[currentIndex].SetActive(false);

        if (firstPersonController != null && firstPersonController.enabled == false)
        {
            firstPersonController.enabled = true;
        }

        if (interactionController != null && interactionController.enabled == false)
        {
            interactionController.enabled = true;
        }

        isInspecting = false;
        //To lock cursor after inspection is complete.
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Confined;
        inspectionCamera.fieldOfView = defaultFieldOfView;
    }
}